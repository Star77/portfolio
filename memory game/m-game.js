
var cards = ['hi','hi','sun','sun','moon','moon','cat','cat','dog','dog','home','home','love','love','bye','bye','ok','ok','LoL','LoL',';)',';)',':(',':('];
var values = [];
var tiles = [];
var flipped_tiles = 0;

Array.prototype.memory_tile_shuffle = function(){
    var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random() * (i+1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
}

function newBoard(){
	flipped_tiles= 0;
	var output = '';
    cards.memory_tile_shuffle();
	for(var i = 0; i < cards.length; i++){
		output += '<div id="tile_'+i+'" onclick="memoryFlipTile(this,\''+cards[i]+'\')"></div>';
	}
	document.getElementById('board').innerHTML = output;
}
function memoryFlipTile(tile,val){
	if(tile.innerHTML == "" && values.length < 2){
		tile.style.background = '#fff';
		tile.innerHTML = val;
		if(values.length == 0){
			values.push(val);
			tiles.push(tile.id);
		} else if(values.length == 1){
			values.push(val);
			tiles.push(tile.id);
			if(values[0] == values[1]){
				flipped_tiles+= 2;
				// Clear both arrays
				values = [];
      tiles = [];

				// Check to see if the whole board is cleared
				if(flipped_tiles== cards.length){
					alert("... generating new board");
					document.getElementById('board').innerHTML = "";
					newBoard();
				}
			} else {
				function flip2Back(){
				    // Flip the 2 tiles back over
				    var tile_1 = document.getElementById(tiles[0]);
				    var tile_2 = document.getElementById(tiles[1]);
				    tile_1.style.background = 'url(k.jpg) no-repeat';
            	    tile_1.innerHTML = "";
				    tile_2.style.background = 'url(k.jpg) no-repeat';
            	    tile_2.innerHTML = "";
				    // Clear both arrays
				    values = [];
            tiles = [];
				}
				setTimeout(flip2Back, 700);
			}
		}
	}
}
